<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('logout', 'Auth\LoginController@logout', function () {
    return abort(404);
});
Auth::routes();

Route::get('/dashboard', 'DashboardController@index')->name('home');
Route::get('/backend/daftar-menu', 'DaftarMenuController@index');
// Route::resource('/backend/daftar-menu', 'DaftarMenuController');
Route::delete('/backend/daftar-menu/{id}', 'DaftarMenuController@destroy');
Route::patch('/backend/daftar-menu/{id}', 'DaftarMenuController@update');
Route::post('/backend/daftar-menu', 'DaftarMenuController@store');
Route::get('/backend/daftar-menu/{id}/edit', 'DaftarMenuController@edit');