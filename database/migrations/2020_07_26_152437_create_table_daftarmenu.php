<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableDaftarmenu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daftarmenu', function (Blueprint $table) {
            $table->bigIncrements('daftarmenu_id');
            $table->string('nama_menu');
            $table->string('harga');
            $table->unsignedBigInteger('jenis_id');
            $table->foreign('jenis_id')->references('jenis_id')->on('jenis')->onDelete('cascade');
            $table->string('keterangan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('daftarmenu', function (Blueprint $table) {
            $table->dropForeign(['jenis_id']);
        });
        Schema::dropIfExists('daftarmenu');
    }
}
