<?php

use Illuminate\Database\Seeder;

class AdministratorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new \App\User;
        $user->username = "superadmin";
        $user->name = "Miftahularif";
        $user->email = "arieftheluffy@gmail.com";
        $user->password = \Hash::make("superadmin");
        $user->level ="superadmin";
        $user->save();
        $this->command->info("User SuperAdmin berhasil dibuat");
    }
}
