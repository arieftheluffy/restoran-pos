<?php

namespace App\Http\Controllers;

use App\DaftarMenu;
use App\Jenis;
use Illuminate\Http\Request;

class DaftarMenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $daftarmenu = DaftarMenu::all();
        $jenis = Jenis::all();
        return view('backend.daftarmenu.index', compact('daftarmenu', 'jenis'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        DaftarMenu::create($input);
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DaftarMenu  $daftarMenu
     * @return \Illuminate\Http\Response
     */
    public function show(DaftarMenu $daftarMenu)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DaftarMenu  $daftarMenu
     * @return \Illuminate\Http\Response
     */
    public function edit(DaftarMenu $daftarMenu, $id)
    {
        // return $daftarMenu;
        $daftaredit = DaftarMenu::findorFail($id);
        return response()->json($daftaredit, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DaftarMenu  $daftarMenu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DaftarMenu $daftarMenu)
    {
        $daftarMenu = DaftarMenu::findOrFail($request->daftarmenu_id);
        $daftarMenu->update($request->all());
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DaftarMenu  $daftarMenu
     * @return \Illuminate\Http\Response
     */
    public function destroy(DaftarMenu $daftarMenu, $id)
    {
        $daftarMenu = DaftarMenu::findOrFail($id);
        $daftarMenu->delete();
        return redirect()->back();
    }
}
