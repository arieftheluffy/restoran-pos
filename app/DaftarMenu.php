<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DaftarMenu extends Model
{
    protected $table = "daftarmenu";
    protected $primaryKey = "daftarmenu_id";
    protected $fillable = [
        'nama_menu','harga', 'jenis_id', 'keterangan'
    ];

    public function jenis()
    {
        return $this->belongsTo('App\Jenis', 'jenis_id');
    }
}
