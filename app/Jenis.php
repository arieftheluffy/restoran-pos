<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jenis extends Model
{
    protected $table = "jenis";
    protected $primaryKey = "jenis_id";
    protected $fillable = [
        'nama_jenis', 'jenis_id', 'keterangan'
    ];
}
