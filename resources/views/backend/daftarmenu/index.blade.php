@extends('layouts.template')

@section('css')
    <!-- DataTables -->
    <link href="{{ URL::asset('assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Sweet Alert-->
    <link href="{{ URL::asset('assets/libs/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="page-content">

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="page-title mb-0 font-size-18">Dashboard</h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                        <li class="breadcrumb-item active">Daftar Menu</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
    <!-- end page title -->

    <!-- row kedua -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Daftar Menu</h4>
                    <p class="card-title-desc">Mohon cek harga makanan dan minuman sebelum klik simpan.
                    </p>
                    <div class="card-title">
                        <a href="javascript:;" onclick="return addDaftarMenu()" class="btn btn-lg btn-primary waves-effect waves-light">Tambah Data</a>
                    </div><br>
                    <table id="datatable" class="table table-bordered table-striped" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                            <tr>
                                <th style="width: 3%;">#</th>
                                <th style="width: 35%;">Daftar Menu</th>
                                <th style="width: 15%;">Harga (Rp.)</th>
                                <th>Jenis</th>
                                <th>Keterangan</th>
                                <th style="width: 1%;">Aksi</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($daftarmenu as $data)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $data->nama_menu }}</td>
                                <td>{{ $data->harga }}</td>
                                <td>{{ $data->jenis->nama_jenis }}</td>
                                <td>{{ $data->keterangan }}</td>
                                <td>
                                    <form action="{{ url('backend/daftar-menu', $data->daftarmenu_id) }}" method="POST">
                                        @csrf
                                        @method("DELETE")
                                        <div class="btn-group">
                                            <a href="javascript:;" class="btn btn-md btn-primary" onclick="return editDaftarMenu({{ $data->daftarmenu_id }})" title="Edit"><i class="fa fa-pencil-alt"></i></a>
                                            <a href="javascript:;" onclick="return showAlert($(this).closest('form'));" class="btn btn-md btn-danger" title="Hapus"><i class="far fa-trash-alt"></i></a>
                                        </div>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
            <!-- end card -->
        </div>
        <!-- end col -->

    </div>
    <!-- end row kedua -->

</div>
<!-- End Page-content -->

<!-- DaftarMenu Modal -->
<div class="modal fade" id="modal-daftarmenu" tabindex="-1" role="dialog" aria-labelledby="modal-daftarmenu" aria-hidden="true">
    <div class="modal-dialog modal-dialog-popin" role="document">
        <form id="modal-daftarmenu-form" method="POST" action="">
            @csrf
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0">
                    <div class="modal-header">
                        <h5 class="modal-title mt-0" id="modal-daftarmenu-title">Tambah Daftar Menu</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="daftarmenu_id" name="daftarmenu_id">
                        <div class="form-group">
                            <label for="nama_menu" class="control-label">Nama Menu<span class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="nama_menu" name="nama_menu" required>
                        </div>
                        <div class="form-group">
                            <label for="harga" class="control-label">Harga/porsi <span class="text-danger">*</span></label>
                            <input type="number" min="0" max="30000" step="1000" class="form-control" id="harga" name="harga" required>
                        </div>
                        <div class="form-group">
                            <label for="jenis_id" class="control-label">Jenis Menu <span class="text-danger">*</span></label>
                            <select class="form-control" name="jenis_id" id="jenis_id">
                                @foreach($jenis as $datajenis)
                                    <option value="{{ $datajenis->jenis_id }}">{{ $datajenis->nama_jenis }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="keterangan" class="control-label">Keterangan <span class="text-danger">*</span></label>
                            <textarea class="form-control" id="keterangan" name="keterangan"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-lg btn-light" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-lg btn-primary">Simpan</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
    <!-- END Mapel Block Modal -->

@endsection

@section('js')
    <!-- Required datatable js -->
    <script src="{{ URL::asset('assets/libs/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>

    <!-- Sweet Alerts js -->
    <script src="{{ URL::asset('assets/libs/sweetalert2/sweetalert2.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function()
        {
            $("#datatable").DataTable()
        });
        jQuery.extend(true, jQuery.fn.dataTable.defaults, {
            language: {
                lengthMenu: "_MENU_",
                search: "_INPUT_",
                searchPlaceholder: "Cari...",
                info: "Halaman <strong>_PAGE_</strong> dari <strong>_PAGES_</strong>",
                paginate: {
                    first: '<i class="fa fa-angle-double-left"></i>',
                    previous: '<i class="fa fa-angle-left"></i>',
                    next: '<i class="fa fa-angle-right"></i>',
                    last: '<i class="fa fa-angle-double-right"></i>'
                }
            }
        });
        jQuery('#datatable').dataTable({
            pageLength: 25,
            lengthMenu: [[10, 25, 50], [10, 25, 50]],
            autoWidth: false
        });

        function showAlert(form) {
            var e = Swal.mixin({
                buttonsStyling: !1,
                customClass: {
                    input: "form-control"
                }
            });

            e.fire({
                title: 'Apakah anda yakin?',
                text: 'Anda tidak akan dapat mengembalikan data anda',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Ya',
                cancelButtonText: 'Tidak',
                customClass: {
                    confirmButton: "btn btn-lg btn-danger m-1",
                    cancelButton: "btn btn-lg btn-secondary m-1"
                },
            }).then((result) => {
                if(result.value) {
                    form.submit();
                }
            });
        }
        function clearModalForm() {
            $("#nama_menu").val("");
            $("#harga").val("");
            $("#keterangan").val("");
            //$("#jenis_id").val("");
        }

        function addDaftarMenu() {
            clearModalForm();
            $("#modal-daftarmenu-form-method").remove();
            $("#modal-daftarmenu-title").html("Tambah Mata Pelatihan");
            $("#modal-daftarmenu").modal('show');
        }

        function editDaftarMenu(id) {
            clearModalForm();
            var url_form = "{{ url('/backend/daftar-menu/:id') }}";
            var url_edit = "{{ url('/backend/daftar-menu/:id/edit') }}";

            url_form = url_form.replace(':id', id);
            url_edit = url_edit.replace(':id', id);

            $("#modal-daftarmenu-title").html("Edit Daftar Menu");
            $("#modal-daftarmenu-form").attr("action", url_form);
            $("#modal-daftarmenu-form-method").remove();
            $('#modal-daftarmenu-form').append('<input type="hidden" id="modal-daftarmenu-form" name="_method" value="patch">');

            $.ajax({
                type: "get",
                url: url_edit,
                success: function(data) {
                    $("#daftarmenu_id").val(data.daftarmenu_id);
                    $("#nama_menu").val(data.nama_menu);
                    $("#harga").val(data.harga);
                    $("#jenis_id").val(data.jenis_id);
                    $("#keterangan").val(data.keterangan);
                    for(var i = 0; i< data.length;i++){
                        //$('#jenis_id').append('<option value="'+data[i]['jenis_id']+'">'+data[i]['nama_jenis']+'</option>');
                        $('#jenis_id').append('<option value="'+data[i]['jenis_id']+'">').prop('selected', true);
                    }
                    $("#modal-daftarmenu").modal('show');
                    console.log(data);
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    alert(textStatus);
                }
            });

            return false;
        }
    </script>

@endsection